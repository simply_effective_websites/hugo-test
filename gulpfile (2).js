var requireDir = require('require-dir');
gutil = require('gulp-util');
var path = require('path');

subfolder = "";

//default_path = 'webbus';

// Require all tasks in gulp, including subfolders
var arg1 = process.argv.slice(2);

gutil.log('Gulp arg is: ' + arg1);

if (arg1 == "") {
	gutil.log('No arg set so defaulting to make_final');
	requireDir('C:/Users/Mike/hugo/gulp/make_final/', { recurse: true });
}
else {
	requireDir('C:/Users/Mike/hugo/gulp/' + arg1 + '/', { recurse: true });
}