+++
type = "block"
weight = 0

title = "Dane kontaktowe"
+++
### Pri Trakt  
Przedsiębiorstwo Robót Inżynieryjnych  
Włodzimierz Lewowski  
e-mail: wlodzimierz.lewowski@pri-trakt.pl  
tel. (75) 742 55 90  
adres: Sędzisław 50, 58-410  

#### Dane do faktury  
NIP: 952-198-25-71  
REGON: 140324010  
KRS 0000244730  
nr konta: 43434343434343  
