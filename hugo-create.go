package main

import (
	"bufio"
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"strings"
	"syscall"
	"text/template"
	"time"
	"encoding/csv"
	"io"
)

var postGalleryTemplate string = `---
title: {{.Title}}
date: "{{.Date}}"
image_name: {{.ImagePath}}
previous_image: {{.PreviousImage}}
next_image: {{.NextImage}}
next_post_path: {{.NextPostPath}}
previous_post_path: {{.PreviousPostPath}}
---
`
type GalleryItem struct {
	Title            string
	Date             string
	ImagePath        string
	NextImage        string
	PreviousImage    string
	NextPostPath     string
	PreviousPostPath string
}

var postDataTemplate string = `---
title: {{.Title}}
date: "{{.Date}}"
categories: [ {{ .Categories }} ]
price: {{ .Price }}
---
{{ .Content }}
`
type DataItem struct {
	Title            string
	Date             string
	Categories		string
	Price		   	string
	Content		string
}


func check(e error) int {
	result := 0
	if e != nil {
		result = 1
		defer func() {
			panic(e)
		}()
	}
	return result
}

func main() {
	if len(os.Args) < 3 {
		fmt.Printf("Usage: %s <Conversion Type> <Source Path or Data Filename> <Destination Section> <Title> [BaseUrl]\r\n", os.Args[0])
		syscall.Exit(1)
	}

	contentPath := "content/"
	conversionType := os.Args[1]
	sourcePath := ""
	datafilename := ""
	staticRoot := ""
	section := ""
	title := ""
	
	if len(os.Args) > 2 {
		if conversionType == "gallery" {
			sourcePath = os.Args[2]
		} else {
			sourcePath = conversionType + "/"
			datafilename = os.Args[2]
			section = datafilename + "/"
			contentPath = contentPath + section
		}
		
		if len(os.Args) > 3 {
			staticRoot = strings.Replace(os.Args[2], "static/", "", 1) + "/"
			section = os.Args[3] + "/"
			contentPath = contentPath + section
			
			if len(os.Args) > 4 {
				title = os.Args[4]
			}
		}

	} else {
		fmt.Printf("There must be a secong argument / attribute")
		os.Exit(1)
	}
	
	baseUrl := "/"
	
	if conversionType == "gallery" {
		gallerymain(sourcePath, staticRoot, section, title, baseUrl, contentPath)
	} else {
		datamain(sourcePath, baseUrl, contentPath, datafilename)
	}
}

func gallerymain(sourcePath string, staticRoot string, section string, title string, baseUrl string, contentPath string) {
	src, err := os.Stat(contentPath)
	if err != nil || !src.IsDir() {
		err = os.Mkdir(contentPath, 0755)
		if err != nil {
			fmt.Printf("content directory not found! Are you in a hugo directory?\r\n")
			os.Exit(1)
		}
	}

	postList, err := ioutil.ReadDir(sourcePath)
	
	//fmt.Printf("%+v", postList)
	
		// debig start - to delete 
		//	fmt.Printf("Source path <%s> not found!\r\n", sourcePath)
		//	os.Exit(1)
		// debug end 
	
	if err != nil {
		fmt.Printf("Source path <%s> not found!\r\n", sourcePath)
		os.Exit(1)
	}

	for index, file := range postList {
		//fmt.Printf("File: ", file)
		previousImage, nextImage := getPreviousAndNextPost(index, postList)
		generateGalleryPost(index, file, staticRoot, contentPath, title, previousImage, nextImage, section, baseUrl)
	}
}

func datamain(sourcePath string, baseUrl string, contentPath string, datafilename string) {
	parsecsv(sourcePath, contentPath, datafilename, baseUrl)
}

func getPreviousAndNextPost(index int, postList []os.FileInfo) (previous os.FileInfo, next os.FileInfo) {
	if index+1 < len(postList) {
		next = postList[index+1]
	}
	if index >= 1 {
		previous = postList[index-1]
	}
	return
}

func stripExtension(baseUri string) (fileName string) {
	extension := path.Ext(baseUri)
	fileName = baseUri[0 : len(baseUri)-len(extension)]
	return
}

func buildPathFromFileInfo(imageFile os.FileInfo, sourcePath string, excludeExtension bool, baseUrl string) (imagePath string) {
	if imageFile != nil {
		fileName := imageFile.Name()
		if excludeExtension {
			fileName = stripExtension(imageFile.Name())
		}
		if baseUrl != "" && !excludeExtension {
			imagePath = baseUrl + "/" + sourcePath + fileName
		} else {
			imagePath = sourcePath + fileName
		}
	}
	return
}

func generateGalleryPost(index int, file os.FileInfo, sourcePath string, contentPath string, title string, previousImage os.FileInfo, nextImage os.FileInfo, section string, baseUrl string) {
	nextImagePath := buildPathFromFileInfo(nextImage, sourcePath, false, baseUrl)
	previousImagePath := buildPathFromFileInfo(previousImage, sourcePath, false, baseUrl)
	nextPostPath := buildPathFromFileInfo(nextImage, section, true, baseUrl)
	previousPostPath := buildPathFromFileInfo(previousImage, section, true, baseUrl)
	currentImagePath := ""
	if baseUrl != "" {
		currentImagePath = baseUrl + "/" + sourcePath + file.Name()
	} else {
		currentImagePath = sourcePath + file.Name()
	}

	galleryItem := GalleryItem{
		Title:            title,
		ImagePath:        currentImagePath,
		Date:             time.Now().Format("2006-01-02"),
		NextImage:        nextImagePath,
		PreviousImage:    previousImagePath,
		NextPostPath:     nextPostPath,
		PreviousPostPath: previousPostPath,
	}

	var buffer bytes.Buffer
	generateGalleryTemplate(galleryItem, &buffer)

	filePath := contentPath + stripExtension(file.Name()) + ".md"
	f, err := os.Create(filePath)
	check(err)
	defer f.Close()
	f.Sync()
	w := bufio.NewWriter(f)
	w.WriteString(buffer.String())
	w.Flush()
}

func generateGalleryTemplate(galleryItem GalleryItem, buffer *bytes.Buffer) {
	t := template.New("post template")
	t, _ = t.Parse(postGalleryTemplate)
	err := t.Execute(buffer, galleryItem)
	check(err)
}

func generateDataPost(index int, contentPath string, name string, sub_name string,  product_name string, price string, desc string, baseUrl string) {

	dataItem := DataItem{}
	//newPath := ""
	newPath := contentPath  //+ "products"
	
	if sub_name == "" {
		dataItem = DataItem{
			Title:            product_name,
			Date:             time.Now().Format("2006-01-02"),
			Categories:  " \"" + name + "\"",
			Price: price,
			Content: 	desc,
		}
		
		//newPath = contentPath + "/" + name
	} else {
		dataItem = DataItem{
			Title:            product_name,
			Date:             time.Now().Format("2006-01-02"),
			Categories:  " \"" + name + "\"" + ", " + "\"" + name + "/" + sub_name + "\" ",
			Price: 		price,
			Content: 	desc,
		}
		//newPath = contentPath + "/" + name + "/" + sub_name
	}
	
	var buffer bytes.Buffer
	generateDataTemplate(dataItem, &buffer)
	
	if _, err := os.Stat(newPath); os.IsNotExist(err) {
		os.Mkdir(newPath,0755)
	}

	//filePath := newPath + "/" + "index.md"
	filePath := newPath + "/" + product_name + ".md"
	f, err := os.Create(filePath)
	check(err)
	defer f.Close()
	f.Sync()
	w := bufio.NewWriter(f)
	w.WriteString(buffer.String())
	w.Flush()
	
}

func generateDataTemplate(dataItem DataItem, buffer *bytes.Buffer) {
	t := template.New("post template")
	t, _ = t.Parse(postDataTemplate)
	err := t.Execute(buffer, dataItem)
	check(err)
}

func parsecsv(sourcePath string, contentPath string, datafilename string, baseUrl string) {
	// setion will be filename of csv
	// title will be record 0 of each unique start of row in csv
	
	file, err := os.Open(sourcePath + datafilename + ".csv")
	if err != nil {
		// err is printable
		// elements passed are separated by space automatically
		fmt.Println("Error:", err)
		return
	}
	
	// automatically call Close() at the end of current method
	defer file.Close()
	// 
	
	reader := csv.NewReader(file)
	// options are available at:
	// http://golang.org/src/pkg/encoding/csv/reader.go?s=3213:3671#L94
	reader.Comma = ','
	lineCount := 0
	//prevName := ""
	//prevSub_Name := ""
	
	for {
		// read just one record, but we could ReadAll() as well
		record, err := reader.Read()
		
		// end-of-file is fitted into err
		if err == io.EOF {
			break
		} else if err != nil {
			fmt.Println("Error:", err)
			return
		}
		
		name := record[0]
		sub_name := record[1]
		product_name := record[3]
		desc := record[7]
		price := record[8]
		
		// record is an array of string so is directly printable
		fmt.Println("Record", lineCount, "is", "category ", name, " and sub-category ", sub_name, " and has", len(record), "fields")
		
		// and we can iterate on top of that
		//for i := 0; i < len(record); i++ {
		//	fmt.Println(" ", record[i])
		//	}
		fmt.Println()
		
		if lineCount != 0 {
			// if name != prevName  {
				//	generateDataPost(lineCount, contentPath, name, "", baseUrl)
				//	prevName = record[0]
				
				//	if sub_name != "" {
					//	generateDataPost(lineCount, contentPath, name, sub_name, baseUrl)
					//	prevSub_Name = record[1]
				//	}
				//	} else {
					//	if sub_name != prevSub_Name {
					//		generateDataPost(lineCount, contentPath, name, sub_name, baseUrl)
					//		prevSub_Name = record[1]
					//	}
				//	}
			}
			
			generateDataPost(lineCount, contentPath, name, sub_name, product_name, price, desc, baseUrl)
		
		lineCount += 1
	}
	
}